// // we have made use of four states
// /*
// the source code provide by the ruser, input handles it 

// output derice from the judge0 compiler, poast the submission of code is handle by coutput


// language _id manages the language we wish to compile 

// user_input : deals the the stdin input given by the user 

// first fetch request Create yoru submission to the judge0 compiler and return a unique token

// We get back the result/output from our submitted code with the help of the unique token in our second fetch request

// Weh ave some JSX inside to render method 



// */

import { wait } from "@testing-library/user-event/dist/utils";
import { Component } from "react";
import "./Compiler.css"; 
export default class Compiler extends Component{

    // initialize the state using the constructor

    constructor(props){


        super(props); 
        this.state={


                input:localStorage.getItem('input')||``,
                output:``,
                language_id:localStorage.getItem('language_id')||62,
                user_input:``,
        };

    }

//     // next task is to capture the events that has occured and its target value is recorded. 
// //  similarly, any user input is also recorded as an user event 
input=(event)=>{
        event.preventDefault(); 
        this.setState({input:event.target.value}); 
        localStorage.setItem('input',event.target.value)
    }; 

userInput = (event)=>{
    event.preventDefault(); 
    this.setState({user_input:event.target.value}); 

}


// // the main task is to get the language_id of the programming language that we want to cmopile 

// // in this case java language is 62


language = (event)=>
   {
    event.preventDefault(); 
    this.setState({language_id: event.target.value})

   }; 


 submit= async(e)=>{ 

    e.preventDefault(); 
    let outputText =document.getElementById("output")

    console.log("***********Language id : ",this.state.language_id)



// using RestCall we post the above information with the api key 

outputText.innerHTML=""; 
outputText.innerHTML+= "Creating Submission....\n"; 
const options = {
	method: 'POST',
	headers: {
	 
		'X-RapidAPI-Host': 'judge0-ce.p.rapidapi.com',
		'X-RapidAPI-Key': '79a436927emsh09f018423fcd661p1d07e3jsn9e5a44e74df8',
        'content-type': 'application/json',
        accept:"application/json",
	},
	body:  JSON.stringify({
        source_code: this.state.input,
        stdin: this.state.user_input,
        language_id: this.state.language_id,
    })
    
};

fetch('https://judge0-ce.p.rapidapi.com/submissions', options)
	 .then(response => response.json())
	.then(response => {

        if(response.token){
            let url=`https://judge0-ce.p.rapidapi.com/submissions/${response.token}?base64_encoded=true`; 

            const getSolution =fetch(url,{
                method:"GET",
                headers:{
                    "x-rapidapi-host":"judge0-ce.p.rapidapi.com",
                    "x-rapidapi-key":"79a436927emsh09f018423fcd661p1d07e3jsn9e5a44e74df8",
                    "content-type":"application/json"
                }
            }
                
                ).then(response=> response.json())
                .
                then(response=> { 
                    
                    console.log("Hahahahaa :",response)
            
 if(response.stdout){
    const output= atob(response.stdout); 
    outputText.innerHTML="";
    outputText.innerHTML += `Results:\n ${output}\n Execution Time:${response.time} Secs\n Memory used: ${response.memory}bytes`;
}else if(response.stderr) {

    const error = atob(response.stderr); 
    outputText.innerHTML=""; 
    outputText.innerHTML +=`\n Error:${error}`
}else {
    const compilation_error = atob(response.compile_output)
    outputText.innerHTML=""; 
    outputText.innerHTML += `\n Error :${compilation_error}; `
}
            
            })

        }
    })
	.catch(err => console.error(err));

}
// const response =  fetch('https://judge0-ce.p.rapidapi.com/submissions', options)
// 	.then(response => {

//         console.log("here is response : ",response)
//         outputText.innerHTML=  `Create Submission ..\nSubmission Create...\n Checking Submission Status\n `
       
    
//         if(response.token){
    
//                 let url=`https://judge0-ce.p.rapidapi.com/submissions/${response.token}?base64_encoded=true`; 
//                 const getSolution=  fetch(url,
//                     {
//                         method:"GET",
//                         headers:{
//                             "x-rapidapi-host":"judge0-ce.p.rapidapi.com",
//                             "x-rapidapi-key":"79a436927emsh09f018423fcd661p1d07e3jsn9e5a44e74df8",
//                             "content-type":"application/json"
//                         }
//                     }).then(response=>{ console.log("Another erposne : ",response)})
             
//         }
//     } 
//     )
 
// 	.catch(err => console.error(err));
   
   
   
    
     


// }

 







//    }



 


    
   render(){

    return (
        <>
        
        <div    className="row container-fluid">

                <div    className="col-6 ml-4">

                    <label htmlFor="solution">

                        <span   className="badge badge-info heading mt-2">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
  <path stroke-linecap="round" stroke-linejoin="round" d="M10 20l4-16m4 4l4 4-4 4M6 16l-4-4 4-4" />
</svg> Code Here
                        </span>
                    </label>


<textarea required name="solution" id="source" onChange={this.input} className="source" value={this.state.input}>
    
</textarea>
<button type="submit" className="btn btn-danger ml-2 mr-2" onClick={this.submit}>

<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
  <path stroke-linecap="round" stroke-linejoin="round" d="M9.75 17L9 20l-1 1h8l-1-1-.75-3M3 13h18M5 17h14a2 2 0 002-2V5a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z" />
</svg> Run
</button>

<label htmlFor="tags">

      <b className="heading"> Languages</b>
          
                        </label>

                        <select
                        value={this.state.language_id} 
                        className="form-control form-inline mb-2 langauge">
                            <option value="54"> C++</option>
                            <option value="50"> C</option>
                            <option value="62"> Java </option>
                            <option value="71"> Python</option>
                        </select>



                </div>

                <div    className="col-5">

                    <div>
                        <span   className=" badge badge-info heading my-2">
                            output
                        </span>

                        <textarea id="output"></textarea>
                    </div>

                </div>

        </div>
        

        <div    className="mt-2 ml-5">
            <span   className="badge badge-primary heading my-2">

            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
  <path stroke-linecap="round" stroke-linejoin="round" d="M9 12h6m-6 4h6m2 5H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z" />
</svg>User Input
            </span>
            <br></br>

            <textarea id="input" onChange={this.userInput}></textarea>

        </div>
        
        </>
    ) ;
};
}